A simple pygame zero game.

# Requirements

python 3.x

pygame zero

# Run

  pgzrun click-the-dots.py

# Gameplay

Click at the blue dots for score.

Click at the green dots for lives.

Avoid the red ones.

# Credits

Programming: Lino Distelrath, Benni Bärmann

Music: CC-BY from playonloop.com
