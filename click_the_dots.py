from math import sin, cos, sqrt, radians, degrees, atan
from random import randint, choice

class Mob:
    x = 400
    y = 400
    angle = 0
    speed = 4

    def move(self):
        self.x += sin(radians(self.angle)) * self.speed
        self.y += cos(radians(self.angle)) * self.speed


    def __init__(self, angle):
        self.angle = angle

    def distance_to(self, pos):
        x_distance = abs(self.x - pos[0])
        y_distance = abs(self.y - pos[1])
        return sqrt(x_distance ** 2 + y_distance ** 2)

    def is_in(self, pos):
        distance = self.distance_to(pos)
        return distance <= self.radius

    def of_screen(self):
        return ((self.x < 0 - self.radius) or
               (self.y < 0 - self.radius) or
               (self.x > WIDTH + self.radius) or
               (self.y > HEIGHT + self.radius))

    def set_easy_angle(self):
        '''calculate an angle away from the nearest borders'''

        diff_left = self.x
        diff_up = self.y
        diff_right = WIDTH - self.x
        diff_down = HEIGHT - self.y
        min_dist = 300
        min_angle = 0
        max_angle = 360

        if diff_down < min_dist:
            min_angle = 90
            max_angle = 270
        if diff_right < min_dist:
            min_angle = 180
        if diff_up < min_dist:
            min_angle = 270
        if diff_left < min_dist:
            max_angle = 180 if diff_up >= min_dist else 90
            min_angle = 0 if diff_down >= min_dist else 90

        # print(self.x, self.y, min_angle, max_angle)
        self.angle = randint(min_angle, max_angle)


class LevelAnimation:
    text = ''
    size = 0
    max_size = 500

    def __init__(self, text):
        # print ('new anim')
        self.text = text
        unschedule()

    def draw(self):
        # print(self.size)
        screen.draw.text(self.text, center =[400, 400], fontsize=self.size, color=(int(self.size/2), 0, 0))

    def update(self):
        global score, anim
        if self.size < self.max_size:
            self.size += 1
        else:
            anim = None
            score += 1
            schedule()


class Dot(Mob):
    radius = 30
    color = (255, 255, 255)

    def draw(self):
        screen.draw.filled_circle((self.x, self.y), self.radius, self.color)

    def move_to_spawn(self):
        self.x = spawn.x
        self.y = spawn.y

    def __init__(self):
        # print('new_dot')
        self.angle = randint(1,360)
        self.move_to_spawn()


class Bouncing:
    def bounce(self):
        if self.x <= 0 + self.radius:
            self.angle -= 2*(self.angle)
        if self.y <= 0 + self.radius:
            self.angle -= 2*(self.angle - 90)
        if self.x >= WIDTH - self.radius:
            self.angle -= 2*(self.angle)
        if self.y >= WIDTH - self.radius:
            # self.angle = 180 - self.angle
            self.angle -= 2*(self.angle - 90)


class EasyDot(Dot):
    def __init__(self):
        self.move_to_spawn()
        if phase ==1:
            self.angle = randint(1,360)
        elif phase == 2:
            ''' we calculate an more easy angle here'''
            self.set_easy_angle()
        elif phase == 3:
            self.angle = 270


class Spawn(Mob, Bouncing):
    speed = 0.5
    radius = 30

    def move(self):
        if phase == 2:
            super().move()
        elif phase == 3:
            self.x = 770
            self.y = randint(30,770)
        elif phase == 4:
            self.x = 400
            self.y = 400


class RedDot(Dot, Bouncing):
    color = (120, 0, 0)
    random_angle = 0

    def clicked(self):
        global lives
        lives -= 1
        Dots.remove(self)

    ''' move the dot away from the given position '''
    def repel(self, x, y):
        repel_speed = 0.1
        dist = self.distance_to((x,y))
        rel = repel_speed / dist
        self.x += (self.x - x) * rel
        self.y += (self.y - y) * rel

    def move(self):
        if phase == 4:
            ''' in phase 4 the red dots should have a swarming behaviour
            arround the boss '''
            boss = Dots[0] # boss should always be at the start of list!
            if boss:
                ''' move the angle towards the boss '''
                xdiff = self.x - boss.x
                ydiff = self.y - boss.y
                new_angle = self.angle
                if ydiff == 0:
                    new_angle = 270 if xdiff > 0 else 90
                elif ydiff < 0:
                    new_angle = degrees(atan(xdiff/ydiff))
                else:
                    if xdiff == 0:
                        new_angle = 180 if ydiff > 0 else 0
                    elif xdiff > 0:
                        new_angle = 270 - degrees(atan(ydiff/xdiff))
                    else:
                        new_angle = 90 - degrees(atan(ydiff/xdiff))

                '''set speed in relations to the distance from the boss'''
                dist = self.distance_to((boss.x, boss.y))
                self.speed = dist/50
                ''' add some randomness to the process '''
                if randint(1,10) == 1:
                    self.random_angle = randint (-90,90)
                self.angle = new_angle + self.random_angle
                if self.random_angle > 0:
                    self.random_angle -= 0.1
                elif self.random_angle < 0:
                    self.random_angle += 0.1
                '''repel from any other red dot'''
                for dot in Dots:
                    if dot is self or type(dot) != RedDot:
                        continue
                    self.repel(dot.x, dot.y)

        super().move()

class BlueDot(EasyDot):
    color = (0, 0, 120)

    def clicked(self):
        global score
        score += 1
        if randint(1, 100) <= 3:
            new_green_dot()
        Dots.remove(self)

    def __init__(self):
        self.move_to_spawn()
        if phase != 2 and phase != 3 :
            self.angle = randint(1,360)
        elif phase == 2:
            ''' moving spawn at phase 2 requires an easy angle '''
            self.set_easy_angle()
        elif phase == 3:
            self.angle = 270
            self.speed = 5



class GreenDot(EasyDot):
    color = (0, 160, 0)
    speed = 5

    def clicked(self):
        global lives
        lives += 1
        Dots.remove(self)



class BossDotOne(Dot, Bouncing):
    color = (0, 160, 160)
    speed = 2.5
    health = 20
    radius = 60

    def clicked(self):
        global phase
        self.health -= 1
        if self.health <= 0:
            phase = 5
            print(phase)
            Dots.remove(self)


''' global functions start here '''
startpoints = []
endpoints = []
gray = 0
def draw_background():
    global startpoints, endpoints, gray
    maxlines = 25
    maxgray = 100

    if startpoints:
        for i in range(0, maxlines):
            startpoints[i][0] += choice((-1,1))
            startpoints[i][1] += choice((-1,1))
            endpoints[i][0] += choice((-1,1))
            endpoints[i][1] += choice((-1,1))
    else:
        for i in range(0, maxlines):
            startpoints.append([randint(0, WIDTH), randint(0, HEIGHT)])
            endpoints.append([randint(0, WIDTH), randint(0, HEIGHT)])
    for i in range(0, maxlines):
        gray += 0.1
        gray %= maxgray
        screen.draw.line(startpoints[i], endpoints[i], (gray, gray, gray))

def draw():
    global anim
    screen.clear()
    draw_background()
    for dot in Dots:
        dot.draw()
    screen.draw.text(str(score),center=[60, 70], fontsize=80)
    draw_lives(lives)
    if anim:
        anim.draw()
    else:
        for dot in Dots:
            dot.draw()
        if gameover:
            screen.draw.text('gameover', center =[400, 400], fontsize=150, color=(125, 0, 0))
            draw_lives(0)


def update():
    global phase, anim, Dots, gameover, lives, boss_dot_one_created

    spawn.bounce()
    if score in (0,30,60,90):
        if not anim:
            phase = int(score/30) + 1
            anim = LevelAnimation(str(phase))
    if not gameover and not anim:
        spawn.move()
        if phase == 4 and not boss_dot_one_created:
            for dot in Dots:
                if type(dot) is BlueDot:
                    Dots.remove(dot)
            # make shure that bossdot is allways at the start of the list
            Dots = [BossDotOne()] + Dots
            boss_dot_one_created = True
        if lives <= 0:
            gameover = True
            music.fadeout(0.5)
            return
        for dot in Dots:
            dot.move()
            if dot.of_screen():
                Dots.remove(dot)
                if type(dot) is BlueDot:
                    lives -= 1
            if type(dot) is RedDot or type(dot) is BossDotOne:
                dot.bounce()
    if anim:
        anim.update()


def new_blue_dot():
    global Dots
    if not gameover:
        if phase != 4:
            Dots.append(BlueDot())
        if phase != 3:
            clock.schedule_unique(new_blue_dot, 1.0- score*0.003)
        else:
            clock.schedule_unique(new_blue_dot, 0.95- score*0.003)


def new_red_dot(scheduled = True):
    global Dots
    if not gameover:
        ret = RedDot()
        Dots.append(ret)
        if scheduled:
            clock.schedule_unique(new_red_dot, 10.0)
        return ret


def new_green_dot():
    global Dots
    if not gameover:
        Dots.append(GreenDot())


def on_mouse_down(pos, button):
    global mouse_pressed
    global lives
    global score
    clicked_dot = False
    if gameover:
        start()
    else:
        if button == mouse.LEFT:
            for dot in Dots:
                if dot.is_in(pos):
                    dot.clicked()
                    clicked_dot = True
            if not clicked_dot:
                new_dot = new_red_dot(False)
                new_dot.x = pos[0]
                new_dot.y = pos[1]


def draw_lives(lives):
    if lives > 0:
        for i in range(0, lives):
            screen.draw.filled_circle((40 + i*20, 20), 7, (0, 110, 0))


def schedule():
    clock.schedule_unique(new_blue_dot,1.0)
    clock.schedule_unique(new_red_dot,10.0)


def unschedule():
    clock.unschedule(new_red_dot)
    clock.unschedule(new_blue_dot)


def start():
    global score, lives, gameover, clock_difference, phase
    global mouse_pressed, spawn, Dots, boss_dot_one_created, anim

    score = 0
    lives = 3
    gameover = False
    clock_difference = 0
    phase = 1
    mouse_pressed = False
    spawn = Spawn(randint(1, 360))
    Dots = []
    boss_dot_one_created = False
    anim = None

    music.play('loop')
    schedule()


''' main program starts here'''
WIDTH = 800
HEIGHT = 800
start()
